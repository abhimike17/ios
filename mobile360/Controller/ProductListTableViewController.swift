//
//  ProductListTableViewController.swift
//  mobile360
//
//  Created by Abhijeeth on 7/5/18.
//  Copyright © 2018 Abhijeeth. All rights reserved.
//

import UIKit

class ProductListTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath)
        
        return cell
    }
    
}
