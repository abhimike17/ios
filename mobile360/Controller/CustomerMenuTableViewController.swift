//
//  CustomerMenuTableViewController.swift
//  mobile360
//
//  Created by Abhijeeth on 7/5/18.
//  Copyright © 2018 Abhijeeth. All rights reserved.
//

import UIKit

class CustomerMenuTableViewController: UITableViewController {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imgAvatar: UIImageView!
    //    @IBOutlet var imgAvatar: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelName.text = User.currentUser.name
        
        //code might throw exception
        imgAvatar.image = try! UIImage(data: Data(contentsOf: URL(string: User.currentUser.pictureURL!)!))
        imgAvatar.layer.cornerRadius = 70/2
        imgAvatar.layer.borderWidth = 0.5
        imgAvatar.layer.borderColor = UIColor.white.cgColor
        imgAvatar.clipsToBounds = true

        view.backgroundColor = UIColor(red: 0.19, green:0.18, blue:0.31, alpha:1.0)
    }
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "CustomerLogout"{
            
            APIManager.shared.logout(completionHandler: { (error) in
                if error == nil{
                    FBManager.shared.logOut()
                    User.currentUser.resetInfo()
                    
                    //rerender loginview once you complete loggin out process
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let appController = storyboard.instantiateViewController(withIdentifier: "MainController")
                        as! LoginViewViewController
                    
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window!.rootViewController = appController
                }
            })
            return false
        }
        return true
    }
   
}

